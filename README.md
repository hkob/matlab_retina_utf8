# Mac 版 Matlab を UTF-8 対応にするための ansible playbook #

デフォルトエンコーディングを UTF-8 に変更します。以前は Retina 対応していない Matlab を修正するパッチも含まれていましたが、これは本家が対応したので外しています。

### Sierra ユーザへの注意 ###
Mathworks から配布されたパッチは、lcdata.xml と lcdata_utf8.xml を書き換えるものでした。このため、パッチ後に再度この ansible を実行し直すことで UTF-8 化することが可能です。ただし、ansible の冪等性から lcdata_sjis.xml が存在すると UTF-8 化を進められないため、以下のコマンドを実行してから ansible を実行してください。

```
#!shell

sudo rm /Applications/MATLAB_R2016b.app/bin/lcdata_sjis.xml
```



### 前提 ###

* Matlab 2016b がインストールされていること
* ansible がインストールされていること (`brew install ansible`)
* リモートのマシンに admin ユーザがいること (site.yml で変更できる)

### 実行方法 ###

* hosts ファイルにマシン一覧を記載します
* シンタックスチェックをします
~~~~
ansible-playbook -i hosts site.yml --syntax-check
~~~~
* Dry-run してみます(admin の password が必要です)
~~~~
ansible-playbook -i hosts site.yml -K --check
~~~~
* ここまでで問題がなければ実際に実行します(admin の password が必要です)
~~~~
ansible-playbook -i hosts site.yml -K
~~~~
* 冪等性があるためもう一度実行しても何も起こりません．